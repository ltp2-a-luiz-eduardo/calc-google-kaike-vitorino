package main

import (
	"encoding/json"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"
)

// Estrutura para a calculadora
type Calculator struct{}

// USAndo servehttp
func (c *Calculator) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// Vendo se o metodo eh get
	if req.Method != "GET" {
		// SE nn for, retorna erro pq o metodo nn eh permitido
		http.Error(res, "500 {code: 500, error: something went wrong}", http.StatusMethodNotAllowed)
		return
	}

	// parametros da utl
	params := req.URL.Query()
	action := params.Get("action") // acao
	num1 := params.Get("num1")     // nujkmero 1
	num2 := params.Get("num2")     //  numero 2

	// VEndo se ta tudo existindo
	if action == "" || num1 == "" || num2 == "" {
		// ERro se tiver falatbnado
		http.Error(res, "400: {result: invalid expression, op: string}", http.StatusBadRequest)
		return
	}

	// MAPra
	operations := map[string]func(float64, float64) float64{
		"add": func(a, b float64) float64 { return a + b },
		"sub": func(a, b float64) float64 { return a - b },
		"mul": func(a, b float64) float64 { return a * b },
		"div": func(a, b float64) float64 {
			if b == 0 {
				http.Error(res, "0 nao pode ser dividido!", http.StatusBadRequest)
				return 0
			}
			return a / b
		},
		"pow": func(a, b float64) float64 { return math.Pow(a, b) },
		"rot": func(a, b float64) float64 { return math.Pow(a, (1 / b)) },
	}

	// TA funcionando: isso diz se a operacao existe
	operationFunc, ok := operations[action]
	if !ok {
		// ERro para acao que =q nn existe
		http.Error(res, "Nao conheco essa matematica ai nao", http.StatusBadRequest)
		return
	}

	// COnverte string para float
	num1Float, err := strconv.ParseFloat(num1, 64)
	if err != nil {
		// erro se o numero 1 nn for um numero
		http.Error(res, "Numero 1 ta errado!", http.StatusBadRequest)
		return
	}

	num2Float, err := strconv.ParseFloat(num2, 64)
	if err != nil {
		// erro se o numero 2 nn for um numero
		http.Error(res, "Numero 2 ta errado!", http.StatusBadRequest)
		return
	}

	// FAz a operacao
	result := operationFunc(num1Float, num2Float)

	// Monta a resposta em formato JSON
	response := struct {
		Result float64 `json:"resultado"`
	}{
		Result: result,
	}

	// Cria o bagulho json para ir
	res.Header().Set("Content-Type", "application/json")
	// Envia para o negocio
	json.NewEncoder(res).Encode(response)
}

func main() {
	// Struct da calculadora
	calculator := &Calculator{}

	// Servidor
	s := &http.Server{
		Addr:         "localhost:8080", // IP do localhost
		Handler:      calculator,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	// Inicia o servidor e ve se tem erro
	log.Fatal(s.ListenAndServe())
}
