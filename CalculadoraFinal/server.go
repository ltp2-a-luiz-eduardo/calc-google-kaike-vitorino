package main

import (
	"log"
	"net/http"
	"time"
)

// Struct do servidor http
type ServidorHTTP struct {
	Calculadora *Calculator    // Instnacia da calculadora para fazer as operacoes matematicas
	Parametros  *ParametrosURL // Instancia que vai lidar com os parametros da url
}

// Funcao que inicia o servidor http
func (s *ServidorHTTP) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		// Utiliza a funcao de resposta de metodo ou path invalido em JSON
		SendInvalidPathResponse(res)
		return
	}

	action, num1, num2, err := s.Parametros.ObterParametros(res, req)
	if err != nil {
		// Utiliza a funcao de resposta de operacao invalida em JSON
		SendInvalidOperationResponse(res, err.Error())
		return
	}

	resultado, err := s.Calculadora.ExecutarOperacao(res, action, num1, num2)
	if err != nil {
		// Utiliza a funcao de resposta de operacao invalida em JSON
		SendInvalidOperationResponse(res, err.Error())
		return
	}

	// Utiliza a funcao de resposta de sucesso em JSON
	SendSuccessResponse(res, resultado, action)
}

// Funcao que da run no servidor
func IniciarServer() {
	calculadora := &Calculator{}   // Criando a instancia da calculadora
	parametros := &ParametrosURL{} // Criando a instancia para ver os parametros da url

	s := &http.Server{
		Addr:         "localhost:8080",
		Handler:      &ServidorHTTP{Calculadora: calculadora, Parametros: parametros},
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	log.Fatal(s.ListenAndServe()) // Inicia o servidor e trata quaisquers erros
}
