package main

import (
	"math"
	"net/http"
)

// Struct para calculadora, ou seja, sua estrutura
type Calculator struct{}

// Aqui se define a interface para as operacoes da calculadora
type Operacoes interface {
	Operacao(a, b float64) float64
}

// Struct para a operacao de adicao ou +
type Add struct{}

// Funcao onde ocorre a soma dos numeros
func (Add) Operacao(a, b float64) float64 {
	return a + b
}

// Struct para a operacao de subtracao ou -
type Sub struct{}

// Funcao onde ocorre a subtracao dos numeros
func (Sub) Operacao(a, b float64) float64 {
	return a - b
}

// Struct para a operacao de multiplica ou *
type Mul struct{}

// Funcao onde ocorre a multiplicacao dos numeros
func (Mul) Operacao(a, b float64) float64 {
	return a * b
}

// Struct para a operacao de divisao
type Div struct{}

// Funcao onde se te a operacao de dibisao dos numeros
func (Div) Operacao(a, b float64) float64 {
	if b == 0 {
		return math.Inf(1)
	}
	return a / b
}

// Struct para a operacao (POW) de exponenciacao ou ^
type Pow struct{}

// Funcao da exponenciacao
func (Pow) Operacao(a, b float64) float64 {
	return math.Pow(a, b)
}

// Struct para a operacao (ROT) raiz
type Rot struct{}

// Fucnao com a operacao de raiz
func (Rot) Operacao(a, b float64) float64 {
	return math.Pow(a, (1 / b))
}

// Mapa ligando as operacaoes especifcas e suas devidas funcoes
var OperacoesMapa = map[string]Operacoes{
	"add": Add{},
	"sub": Sub{},
	"mul": Mul{},
	"div": Div{},
	"pow": Pow{},
	"rot": Rot{},
}

// Funcao que executa a operacao do mapa
func (c Calculator) ExecutarOperacao(res http.ResponseWriter, action string, num1, num2 float64) (float64, error) {
	operacao, ok := OperacoesMapa[action]
	if !ok { //Se !ok, ele retorna erro pq a operacao escolhida nao existe no codigo
		//  Usando funcao SendInvalidOperationResponse para enviar uma resposta de operacao invalida em JSON
		SendInvalidOperationResponse(res, "Operação inválida: "+action)
		return 0, nil
	}
	// Retorna o resultado
	resultado := operacao.Operacao(num1, num2)
	// Utiliza a função SendSuccessResponse do modulo json_response para enviar uma resposta de sucesso em JSON
	//SendSuccessResponse(res, resultado, action)
	return resultado, nil
}
